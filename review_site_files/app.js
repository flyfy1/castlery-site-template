Array.prototype.max = function() {
  return Math.max.apply(null, this);
};

Array.prototype.min = function() {
  return Math.min.apply(null, this);
};
// ==============================================
// Pointer abstraction
// ==============================================

/**
 * This class provides an easy and abstracted mechanism to determine the
 * best pointer behavior to use -- that is, is the user currently interacting
 * with their device in a touch manner, or using a mouse.
 *
 * Since devices may use either touch or mouse or both, there is no way to
 * know the user's preferred pointer type until they interact with the site.
 *
 * To accommodate this, this class provides a method and two events
 * to determine the user's preferred pointer type.
 *
 * - getPointer() returns the last used pointer type, or, if the user has
 *   not yet interacted with the site, falls back to a Modernizr test.
 *
 * - The mouse-detected event is triggered on the window object when the user
 *   is using a mouse pointer input, or has switched from touch to mouse input.
 *   It can be observed in this manner: $jq(window).on('mouse-detected', function(event) { // custom code });
 *
 * - The touch-detected event is triggered on the window object when the user
 *   is using touch pointer input, or has switched from mouse to touch input.
 *   It can be observed in this manner: $jq(window).on('touch-detected', function(event) { // custom code });
 */

var PointerManager = {
    MOUSE_POINTER_TYPE: 'mouse',
    TOUCH_POINTER_TYPE: 'touch',
    POINTER_EVENT_TIMEOUT_MS: 500,
    standardTouch: false,
    touchDetectionEvent: null,
    lastTouchType: null,
    pointerTimeout: null,
    pointerEventLock: false,

    getPointerEventsSupported: function() {
        return this.standardTouch;
    },

    getPointerEventsInputTypes: function() {
        if (window.navigator.pointerEnabled) { //IE 11+
            //return string values from http://msdn.microsoft.com/en-us/library/windows/apps/hh466130.aspx
            return {
                MOUSE: 'mouse',
                TOUCH: 'touch',
                PEN: 'pen'
            };
        } else if (window.navigator.msPointerEnabled) { //IE 10
            //return numeric values from http://msdn.microsoft.com/en-us/library/windows/apps/hh466130.aspx
            return {
                MOUSE:  0x00000004,
                TOUCH:  0x00000002,
                PEN:    0x00000003
            };
        } else { //other browsers don't support pointer events
            return {}; //return empty object
        }
    },

    /**
     * If called before init(), get best guess of input pointer type
     * using Modernizr test.
     * If called after init(), get current pointer in use.
     */
    getPointer: function() {
        // On iOS devices, always default to touch, as this.lastTouchType will intermittently return 'mouse' if
        // multiple touches are triggered in rapid succession in Safari on iOS
        if(Modernizr.ios) {
            return this.TOUCH_POINTER_TYPE;
        }

        if(this.lastTouchType) {
            return this.lastTouchType;
        }

        return Modernizr.touch ? this.TOUCH_POINTER_TYPE : this.MOUSE_POINTER_TYPE;
    },

    setPointerEventLock: function() {
        this.pointerEventLock = true;
    },
    clearPointerEventLock: function() {
        this.pointerEventLock = false;
    },
    setPointerEventLockTimeout: function() {
        var that = this;

        if(this.pointerTimeout) {
            clearTimeout(this.pointerTimeout);
        }

        this.setPointerEventLock();
        this.pointerTimeout = setTimeout(function() { that.clearPointerEventLock(); }, this.POINTER_EVENT_TIMEOUT_MS);
    },

    triggerMouseEvent: function(originalEvent) {
        if(this.lastTouchType == this.MOUSE_POINTER_TYPE) {
            return; //prevent duplicate events
        }

        this.lastTouchType = this.MOUSE_POINTER_TYPE;
        $jq(window).trigger('mouse-detected', originalEvent);
    },
    triggerTouchEvent: function(originalEvent) {
        if(this.lastTouchType == this.TOUCH_POINTER_TYPE) {
            return; //prevent duplicate events
        }

        this.lastTouchType = this.TOUCH_POINTER_TYPE;
        $jq(window).trigger('touch-detected', originalEvent);
    },

    initEnv: function() {
        if (window.navigator.pointerEnabled) {
            this.standardTouch = true;
            this.touchDetectionEvent = 'pointermove';
        } else if (window.navigator.msPointerEnabled) {
            this.standardTouch = true;
            this.touchDetectionEvent = 'MSPointerMove';
        } else {
            this.touchDetectionEvent = 'touchstart';
        }
    },

    wirePointerDetection: function() {
        var that = this;

        if(this.standardTouch) { //standard-based touch events. Wire only one event.
            //detect pointer event
            $jq(window).on(this.touchDetectionEvent, function(e) {
                switch(e.originalEvent.pointerType) {
                    case that.getPointerEventsInputTypes().MOUSE:
                        that.triggerMouseEvent(e);
                        break;
                    case that.getPointerEventsInputTypes().TOUCH:
                    case that.getPointerEventsInputTypes().PEN:
                        // intentionally group pen and touch together
                        that.triggerTouchEvent(e);
                        break;
                }
            });
        } else { //non-standard touch events. Wire touch and mouse competing events.
            //detect first touch
            $jq(window).on(this.touchDetectionEvent, function(e) {
                if(that.pointerEventLock) {
                    return;
                }

                that.setPointerEventLockTimeout();
                that.triggerTouchEvent(e);
            });

            //detect mouse usage
            $jq(document).on('mouseover', function(e) {
                if(that.pointerEventLock) {
                    return;
                }

                that.setPointerEventLockTimeout();
                that.triggerMouseEvent(e);
            });
        }
    },

    init: function() {
        this.initEnv();
        this.wirePointerDetection();
    }
};

// ==============================================
// PDP - image zoom - needs to be available outside document.ready scope
// ==============================================

var ProductMediaManager = {
    IMAGE_ZOOM_THRESHOLD: 20,
    imageWrapper: null,

    swapImage: function(targetImage) {

        targetImage = $jq(targetImage);
        targetImage.addClass('gallery-image');

        var imageGallery = $jq('.product-image-gallery');

        if(targetImage[0].complete) { //image already loaded -- swap immediately

            imageGallery.find('.gallery-image').removeClass('visible');

            //move target image to correct place, in case it's necessary
            imageGallery.append(targetImage);

            //reveal new image
            targetImage.addClass('visible');

        } else { //need to wait for image to load

            //add spinner
            imageGallery.addClass('loading');

            //move target image to correct place, in case it's necessary
            imageGallery.append(targetImage);

            //wait until image is loaded
            imagesLoaded(targetImage, function() {
                //remove spinner
                imageGallery.removeClass('loading');

                //hide old image
                imageGallery.find('.gallery-image').removeClass('visible');

                //reveal new image
                targetImage.addClass('visible');

            });

        }
        //$jq.each(selectedLabels, function(key, value){
        //
        //    //reset all images
        //    $jq(".product-image-gallery li.slide").css("display", "none");
        //    $jq(".product-image-gallery li:not(.slide)").removeClass("active").addClass("hidden");
        //
        //    var image_found = false;
        //    $jq(".product-image-gallery li").each(function(){
        //        var label_val = $jq(this).data("label");
        //        if (typeof label_val != "undefined" ){
        //            if (label_val.toLowerCase() == value){
        //                image_found = true;
        //                $jq(this).removeClass("hidden").addClass("active");
        //            }
        //        }
        //    });
        //
        //    if (!image_found){
        //        $jq(".product-image-gallery li.slide:first-child").css("display", "block");
        //    }
        //});


    },

    wireThumbnails: function() {
        //trigger image change event on thumbnail click
        $jq('.product-image-thumbs .thumb-link').click(function(e) {
            e.preventDefault();
            var jlink = $jq(this);
            var target = $jq('#image-' + jlink.data('image-index'));

            ProductMediaManager.swapImage(target);
        });
    },

    init: function() {
        ProductMediaManager.imageWrapper = $jq('.product-img-box');

        // Re-initialize zoom on viewport size change since resizing causes problems with zoom and since smaller
        // viewport sizes shouldn't have zoom

        //ProductMediaManager.wireThumbnails();

        $jq(document).trigger('product-media-loaded', ProductMediaManager);
    }
};

//Fix Prototype and Bootstrap Conflict
if (Prototype.BrowserFeatures.ElementExtensions) {
    var disablePrototypeJS = function (method, pluginsToDisable) {
            var handler = function (event) {
                event.target[method] = undefined;
                setTimeout(function () {
                    delete event.target[method];
                }, 0);
            };
            pluginsToDisable.each(function (plugin) {
                $jq(window).on(method + '.bs.' + plugin, handler);
            });
        },
        pluginsToDisable = ['collapse', 'dropdown', 'modal', 'tooltip', 'popover', 'tab'];
    disablePrototypeJS('show', pluginsToDisable);
    disablePrototypeJS('hide', pluginsToDisable);
}

$jq( document ).ready(function() {

    PointerManager.init();
    
    // $jq('.product-promo-content a').hover(function(){
    //     $jq(this).find(".product-promo-overlay").show();
    // },function(){
    //     $jq(this).find(".product-promo-overlay").hide();
    // });

    /*  More proper way to bind tooltip event
        This way dynamically generated element still binded to the event
    */
    $jq("body").tooltip({ 
        selector: '[data-toggle="tooltip"]' 
    });

    $jq('.header-menu').eq(0).find('a').click(function(){
        $jq(this).toggleClass("on");
        $jq('.header-content-menu').toggleClass('selected');
    });
    $jq('.header-menu').eq(0).find('a').hover(function(){
        $jq(this).css('width','89px').find('i').stop(true,true).fadeIn('slow');
    },function(){
        $jq(this).css('width','89px').find('i').stop(true,true).fadeOut('slow');
    });

    $jq('#trigger-overlay').click(function(){
        $jq('.overlay-keywords input').val("").focus();
    });

    /* signup-login */
	/*
    var windowHeight = $jq(window).height();
    var headerHeight = $jq('.header').height();
    var footerShareHeight = $jq('.ma-footer-share').height();
    var footerHeight = $jq('.ma-footer-static').height();
    var signupLoginHeight = $jq('.signup-login').height();
    var signupLoginBodyHeight = windowHeight-headerHeight-footerShareHeight-footerHeight-70;

    if(signupLoginHeight <= signupLoginBodyHeight){
        var windowHeight = $jq(window).height();
        var headerHeight = $jq('.header').height();
        var footerHeight = $jq('.ma-footer-static').height();
        var signupLoginHeight = $jq('.signup-login').height();
        $jq('.signup-login').height(windowHeight-headerHeight-footerShareHeight-footerHeight-70);
    }*/

    var signupLi = $jq('.signup-login-form li'),
        signupContent = $jq('.signup-login-form-tabs'),
        index;

    signupLi.click(function(){
        $jq(this).addClass('focus').find('p').show().parents('li').siblings('li').removeClass('focus').find('p').hide();
        index = signupLi.index($jq(this));
        signupContent.eq(index).show().siblings().hide();
        $jq('.signup-login-header h2').eq(index).show().siblings().hide();
    });
    $jq('.signup-login-form-link').click(function(){
        $jq(this).parents('.signup-login-form-tabs').hide().siblings('.signup-login-form-tabs-forgot').show();
        $jq('.signup-login-header h2').eq(2).show().siblings().hide();
    });
    $jq('.signup-login-form-back').click(function(){
        $jq(this).parents('.signup-login-form-tabs-forgot').hide().siblings('.signup-login-form-tabs-login').show();
        $jq('.signup-login-header h2').eq(1).show().siblings().hide();
    });
    $jq('.signup-login-form li label').eq(1).click(function(){
        $jq('.signup-login-form-tabs-forgot').hide().siblings('.signup-login-form-tabs-login').show();
    });
    $jq('.signup-login').parents('.main').removeClass();

    $jq('.signup-login-button button').click(function(){
        $jq('.confirmation').siblings("input").val($jq('.confirmation').val());
    });

    if ($jq('.appointments-content').length>0) {
        $jq('.appointments-info .product-shop-add').click(function(){
            var appContent = $jq('.appointments-content').offset().top;

            var viewportWidth = $jq(window).width();
            // Starting window width when the phone menu start to be displayed
            var headerPhoneMenuStartWidth = 1024;
            if(viewportWidth <= headerPhoneMenuStartWidth) {
                var headerHeight = $jq('.header-phone-menu').height();
                appContent -= headerHeight - 2;
            }

            $jq("html,body").animate({ scrollTop: appContent }, 600);
        });
    }

    /* product shop color */
    $jq('.product-options-img li').click(function(){
        $jq('.product-options-img li').removeClass('on');
        $jq(this).addClass('on');
    });

    /* product shop quantity */

    function addCartUpdateListener(buttonClass, validatorFunc, evalFunc){
        $jq('.' + buttonClass).each(function (idx, ele) {
            var btnEle = $jq(ele);
            var valueInputElem = btnEle.parent().find('.product-shop-quantity-value');
            var valueShow = btnEle.parent().find('.product-shop-quantity-show');
            var qtyInc = parseInt(valueInputElem.attr('qtyInc'));
            var inputName = valueInputElem.attr('name');
            var updatingFields = $jq('.product-shop-quantity-value[name="' + inputName +'"]')

            btnEle.click(function () {
                var v = parseInt(valueInputElem.val());
                if (validatorFunc(v, qtyInc)) {
                    updatingFields.attr('value', evalFunc(v, qtyInc));
                    valueShow.text(evalFunc(v, qtyInc));
                }
            });
        });
    }

    addCartUpdateListener(
        'product-shop-quantity-cut',
        function(v, qtyInc){return v > qtyInc},
        function(v, qtyInc){return v - qtyInc}
    );
    addCartUpdateListener(
        'product-shop-quantity-add',
        function(){return true},
        function(v, qtyInc){ return v + qtyInc}
    );

    /* product shop select */
    $jq('.product-shop-list-info-select-box').each(function(){
        var bundle_option_id = $jq(this).attr("id");
        var bundle_selected_value = bundle.config.selected[bundle_option_id.split("-")[2]];

        //if bundle_selected_value exist, show the selection name, otherwise show the first one
        if (bundle_selected_value != ""){
            var productShopListInfoSelectBoxName=$jq(this).find('li').first().find('.product-shop-list-info-select-name').html();
            $jq(this).parents('.product-shop-list-info').siblings('.product-shop-list-label-select').find('span').html(productShopListInfoSelectBoxName);
        }
     });  
    $jq('.product-shop-list-info-select-box li').click(function(){
        var selection_box = $jq(this).parent().parent();
        var bundle_option_id = $jq(this).data("bundle-id");
        $jq("." + bundle_option_id + "-input").val($jq(this).data("value"));
        if (typeof bundle != undefined){
            bundle.dropdownSelection(bundle_option_id, $jq(this).data("value").toString());
            var leadtime = bundle.getLeadTime(bundle.config.selected);
            if(leadtime){
                var date_str = moment().add(leadtime, "days").format("MMM DD") + "-" + moment().add(leadtime+7, "days").format("MMM DD");
                $jq(".product-shop-list-info-time").html(date_str);
            }
        }
        var productShopListInfoSelectBoxImgs=$jq(this).find('img').attr('src');
        var productShopListInfoSelectBoxNames=$jq(this).find('.product-shop-list-info-select-name').html();
        //alert(productShopListInfoSelectBoxName);
        $jq(this).parents('.product-shop-list-info-select-box').siblings('.select-span').find('img').attr('src',productShopListInfoSelectBoxImgs);
        $jq(this).parents('.product-shop-list-info').siblings('.product-shop-list-label-select').find('span').html(productShopListInfoSelectBoxNames);
    });

    $jq('.add-to-box .product-shop-add').on('click', function () {
        var btn = $jq(this).button('loading');
        // If send successfully, then reset
        //btn.button('reset');
    });

    $jq( document ).on('mouseover', '.add-to-links .link-wishlist.remove-wishlist', function(){
        var btn = $jq(this).button('hover');
    });

    $jq( document ).on('mouseout', '.add-to-links .link-wishlist.remove-wishlist', function(){
        var btn = $jq(this).button('reset');
    });

    $jq( document ).on('mouseover', '.customer-wishlist .unfavourite-btn', function(){
        var btn = $jq(this).button('hover');
    });

    $jq( document ).on('mouseout', '.customer-wishlist .unfavourite-btn', function(){
        var btn = $jq(this).button('reset');
    });

    $jq('.product-list-line').each(function() {
        $jq(this).children('.product-list-amount').matchHeight();
    });

    $jq('.newNav .dropdown').click(function(){
        $jq(this).toggleClass("open");
    });

    $jq('img.lazy').unveil();
    // $jq('.menu-list-content .menu-list-content-name-mobile').bind('click',function(){
    //     $jq(this).parent().siblings('dd').toggle();
    //     $jq(this).siblings('.aw-ln-filter-toggle').find('span').toggle();
    // });

    // $jq( document ).on('hover', '.header-content-shoppingCart',function(){
    //     $jq(this).find('.header-content-shoppingCart-outside').show();
    // }, function(){
    //     $jq(this).find('.header-content-shoppingCart-outside').hide();
    // }

    if ($jq('.sticky-block-content').length > 0 && $jq(window).width > 767) {
        var stickyBlockContentTop = $jq('.sticky-block-content').offset().top;
        $jq(window).scroll(function(){
                var scroH = $jq(this).scrollTop();
                if (scroH >= stickyBlockContentTop){
                    $jq(".sticky-block-content").addClass("on");
                }else{
                    $jq(".sticky-block-content").removeClass("on");
                }
            });
    };

    $jq('.bx-viewport, .more-views #bx-images-pager').hover(function(){
        $jq(this).parent().siblings('.cslr-slider-controls').find('a').show();
    },function(){
        $jq(this).parent().siblings('.cslr-slider-controls').find('a').hide();
    });
    $jq('.cslr-slider-controls a').hover(function(){
        $jq('.cslr-slider-controls a').show();
    });
    if ($jq('.product-view').length >0) {
        if ($jq('.more-views a').length<2) {
            $jq('.more-views a').hide();
        };
    }

     $jq('.nav-header-li').click(function(){
         //$jq(this).find('.tab-content').height($jq(this).find('ul').height());
        var obj = $jq(this).find('.tab-content');
            if (obj.is(":visible")){
            obj.slideUp();    
        } else {
            obj.slideDown();    
        }
         $jq(this).toggleClass('active');
     });
    // var count = 0;
    //   $(".nav-header-li").click(function(){
    //       $(this).toggleClass("highlight", count++ % 3 == 0);
    //   });

    $jq('.owl-carousel-product').owlCarousel({
        items : 4,
        itemsMobile : [414,2],
        itemsDesktopSmall : [1140,4]
    });

    $jq('.about-gallery-carousel').owlCarousel({
        items : 1
    });

    $jq(".swatch-actions .product-shop-add.swatch-to-cart").on("click", function(){
        var btn = $jq(this);
        var productId = btn.data('product');

        // Reference for updating all button (in normal page and in modal)
        var buttons = $jq("button[data-product='" + productId + "']");
        buttons.each(function(idx) {
                $jq(this).button("loading");
            }
        );

        var data = {
            "product": $jq(this).data("product"),
            "qty"    : 1,
            "ajax_cartsuper" : 1
        }
        var message = $jq(this).parent().find(".swatch-add-message");

        // Reference for closing the modal when swatch successfully added to cart
        var modalElem = $jq('#modal-'+productId);

        // Get common parent between the buttons element (to add check icon over the swatch image)
        var commonParent = $jq(buttons[0]).parents().has($jq(buttons[1])).first();
        var imageOverlay = commonParent.find(".swatch-overlay");
        var hoverOverlay = commonParent.find(".hover-overlay");

        $jq.ajax({
            url: $jq(this).data("url"),
            dataType: 'json',
            type : 'post',
            data : data,
            //crossDomain: true,
            beforeSend: function(){
                
            },
            complete: function() {
                //btn.button("reset");
            },
            success: function(data){
                console.log(data.status);
                if(data.status == 1) {
                    buttons.each(function(idx) {
                            $jq(this).text("Added");
                        }
                    );

                    if(data.mini_cart) {
                        $jq('#mini_cart_block').html('');
                        $jq('#mini_cart_block').html(data.mini_cart);
                    }

                    if (data.swatches_cart){
                        $jq('#swatches-cart').html('');
                        $jq('#swatches-cart').html(data.swatches_cart);
                    }

                    $jq('[data-hover="dropdown"]').dropdownHover();

                    // Add check icon over the image
                    imageOverlay.addClass("swatch-added");
                    hoverOverlay.html("");


                    modalElem.modal('hide');
                } else {
                    message.text(data.message);
                    buttons.each(function(idx) {
                        $jq(this).button("reset");
                    }
                );
                }
                return false;
            },
            error:function(){
                buttons.each(function(idx) {
                        $jq(this).button("reset");
                    }
                );
                message.text("Oops! Try to add again.");
            }
        });
    });

    $jq('.btn-checkout.product-shop-add').on("click", function(){
        fbq('track', 'InitiateCheckout');
    });
});


$jq(document).ready(function() {
    ProductMediaManager.init();
    $jq(document).trigger('product-media-loaded');
    $jq("#bx-images-pager a").on("click", function(){
       $jq(".product-image-gallery .gallery-image").removeClass("visible");
    });

    $jq(".col-1 .product-shop-add").on("click", function(){
        $jq(this).button('loading');
    })

    $jq(".actions .product-shop-add").on("click", function(){
        $jq(this).button('loading');
    })

    $jq('.swatch-link').click(function() {
        $jq(this).closest(".product-config-list-info-select").find('#selected-swatch-span').dropdown("toggle");
    });

    var centerModal = function() {
        $jq(this).css('display', 'block');
        var dialog = $jq(this).find(".modal-dialog");
        var offset = ($jq(window).height() - dialog.height()) / 2;
        // Center modal vertically in window
        dialog.css("margin-top", offset);
    }

    var addChatOffset = function(){
        var chatright = parseInt($jq("#livechat-compact-container").css("right"));
        var backtotopright = parseInt($jq("#back-top").css("right"));
        $jq("#livechat-compact-container").css("right", chatright+15);
        $jq("#back-top").css("right", backtotopright+15);
        $jq("#back-top").addClass("notransition");
    }

    var removeChatOffset = function(){
        var chatright = parseInt($jq("#livechat-compact-container").css("right"));
        var backtotopright = parseInt($jq("#back-top").css("right"));
        $jq("#livechat-compact-container").css("right", chatright-15 );
        $jq("#back-top").css("right", backtotopright-15);
    }

    $jq('.product-carousel-modal').on('shown.bs.modal', centerModal);
    $jq('.product-carousel-modal').on('show.bs.modal', addChatOffset);
    $jq('.product-carousel-modal').on('hidden.bs.modal', removeChatOffset);
    $jq(window).on("resize", function () {
        $jq('.modal:visible').each(centerModal);
    });
});


/**
 * A manager to display notification at
 * the top (between header menu and page content)
 */
var notificationManager = {
    TEMPLATE_START:     "<div class='messages'>" +
                            "<div class='alert' role='alert'>",
    TEMPLATE_END:           "</div>" +
                        "</div>",

    displayNotification: function(message, duration) {
        // Default duration (2 sec)
        duration = typeof duration !== 'undefined' ? duration : 2000;

        var htmlStr = this.TEMPLATE_START + message + this.TEMPLATE_END;
        var notifElem = $jq(htmlStr);

        notifElem.clone()
            .hide()
            .prependTo('.container-inner')
            .slideDown()
            .delay(duration)
            .slideUp("normal", function() {
                $jq(this).remove(); // Remove DOM element after all animation finished
            });
    }
}